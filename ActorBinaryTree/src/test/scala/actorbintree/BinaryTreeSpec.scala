package actorbintree

import actorbintree.BinaryTreeSet._
import akka.actor.ActorSystem
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit}
import org.scalatest._

import scala.concurrent.duration._

class BinaryTreeSpec extends TestKit(ActorSystem("BinaryTreeTest")) with DefaultTimeout with ImplicitSender with FlatSpecLike with Matchers with BeforeAndAfterAll {

  import akka.actor.Props

  override def afterAll {
    shutdown()
  }

  val set = system.actorOf(Props[BinaryTreeSet], "BinaryTreeSet")

  "TreeSet" should "behave like system provided Set" in {
    within(500 millis) {
      set ! Insert(self, 1, 8)
      expectMsg(OperationFinished(1))
    }

    within(500 millis) {
      set ! Insert(self, 2, 3)
      expectMsg(OperationFinished(2))
    }

    within(500 millis) {
      set ! Insert(self, 3, 11)
      expectMsg(OperationFinished(3))
    }

    within(500 millis) {
      set ! Remove(self, 4, 3)
      expectMsg(OperationFinished(4))
    }

    set ! GC

    within(500 millis) {
      set ! Insert(self, 5, 5)
      expectMsg(OperationFinished(5))
    }

    within(500 millis) {
      set ! Contains(self, 6, 5)
      ContainsResult(6, true)
    }

    within(500 millis) {
      set ! Contains(self, 7, 3)
      ContainsResult(7, false)
    }
  }
}
