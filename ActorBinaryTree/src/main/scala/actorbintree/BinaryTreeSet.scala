/**
  * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
  */
package actorbintree

import akka.actor._

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef

    def id: Int

    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection */
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply

  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor with ActorLogging with Stash {

  import BinaryTreeNode._
  import BinaryTreeSet._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  def receive = normal(createRoot)

  /** Accepts `Operation` and `GC` messages. */
  def normal(root: ActorRef): Receive = {
    case op: Operation ⇒
      root ! op
    case GC ⇒
      println(s"${self.path.name}: Received GC")
      val newRoot = createRoot
      root ! CopyTo(newRoot)
      context.become(garbageCollecting(newRoot))
  }

  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case CopyFinished ⇒
      println("BinaryTreeSet: Received CopyFinished")
      unstashAll()
      context.become(normal(newRoot))
    case msg ⇒
      println(s"BinaryTreeSet: Received message to be stashed: $msg")
      stash()
  }
}

object BinaryTreeNode {

  trait Position

  case object Left extends Position

  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)

  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode], elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor with ActorLogging {

  import BinaryTreeNode._
  import BinaryTreeSet._

  def receive = normal(initiallyRemoved)

  val possiblePosition: Operation ⇒ Position = (pos) ⇒ if (pos.elem < elem) Left else Right

  /** Handles `Operation` messages and `CopyTo` requests. */
  def normal(removed: Boolean, subtrees: Map[Position, ActorRef] = Map.empty): Receive = {
    case insert@Insert(requester, id, el) if el == elem ⇒
      println(s"${self.path.name}: Inserting $el by flipping removed bit")
      context.become(normal(removed = false, subtrees))
      requester ! OperationFinished(id)
    case insert@Insert(requester, id, el) ⇒
      val pos = possiblePosition(insert)
      if (subtrees.contains(pos)) {
        println(s"${self.path.name}: Inserting ${insert.elem} by forwarding to $pos child")
        subtrees(pos) ! insert
      } else {
        println(s"${self.path.name}: Inserting ${insert.elem} by creating $pos child")
        context.become(normal(removed, subtrees + (pos → context.actorOf(
          BinaryTreeNode.props(insert.elem, initiallyRemoved = false), s"TreeNode-${insert.elem}"))))
        insert.requester ! OperationFinished(insert.id)
      }

    case Contains(requester, id, el) if !removed && el == elem ⇒
      requester ! ContainsResult(id, result = true)
    case contains: Contains ⇒
      val pos = possiblePosition(contains)
      if (subtrees.contains(pos))
        subtrees(pos) ! contains
      else
        contains.requester ! ContainsResult(contains.id, result = false)

    case Remove(requester, id, el) if el == elem ⇒
      println(s"${self.path.name}: Removing $el by flipping removed bit")
      context.become(normal(removed = true, subtrees))
      requester ! OperationFinished(id)
    case remove: Remove ⇒
      val pos = possiblePosition(remove)
      if (subtrees.contains(pos)) {
        println(s"${self.path.name}: Removing ${remove.elem} by forwarding to $pos child")
        subtrees(pos) ! remove
      }
      else {
        println(s"${self.path.name}: No need to remove ${remove.elem}, it is not in the set")
        remove.requester ! OperationFinished(remove.id)
      }

    case copyTo@CopyTo(treeNode) ⇒
      println(s"${self.path.name}> CopyTo($treeNode): removed: $removed, left: ${subtrees.get(Left)}, right: ${subtrees.get(Right)}")
      if (removed) {
        if (subtrees.isEmpty) {
          copyFinished()
        } else {
          subtrees.values.foreach(_ ! copyTo)
          context.become(copying(subtrees.values.toSet, insertConfirmed = true))
        }
      }
      else {
        if (subtrees.isEmpty) {
          treeNode ! Insert(self, id = elem, elem)
          context.become(copying(Set.empty, insertConfirmed = false))
        } else {
          treeNode ! Insert(self, id = elem, elem)
          subtrees.values.foreach(_ ! copyTo)
          context.become(copying(subtrees.values.toSet, insertConfirmed = false))
        }
      }
  }

  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case CopyFinished ⇒
      val nowExpecting = expected - sender()
      if (nowExpecting.isEmpty && insertConfirmed)
        copyFinished()
      else
        context.become(copying(nowExpecting, insertConfirmed))
    case OperationFinished(_) if expected.isEmpty ⇒
      copyFinished()
    case OperationFinished(_) ⇒
      context.become(copying(expected, insertConfirmed = true))
  }

  private def copyFinished(): Unit = {
    println(s"${
      self.path.name
    }: Becoming 'copyFinished'. Sending 'CopyFinished' to ${
      context.parent.path.name
    }")
    context.parent ! CopyFinished
    context.stop(self)
  }
}
