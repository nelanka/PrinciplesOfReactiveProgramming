import actorbintree.BinaryTreeSet
import actorbintree.BinaryTreeSet.{OperationFinished, Insert}
import akka.actor.{ActorSystem, Inbox, Props}
import akka.testkit.TestProbe

object TestApp extends App {
  implicit val system = ActorSystem("Test")
  val set = system.actorOf(Props[BinaryTreeSet])
  val p = TestProbe()
  p.send(set, Insert(p.ref, id=100, 1))
  p.expectMsg(OperationFinished(id=10))


  //  val system = ActorSystem("Test")
  //  val set = system.actorOf(Props[BinaryTreeSet])
  //  val inbox = Inbox.create(system)
  //  set ! Insert(inbox.getRef(), id=100, 1)
  system.terminate()
}


/*
  Insert(requesterRef, id=100, 1),
  Contains(requesterRef, id=50, 2),
  Remove(requesterRef, id=10, 1),
  Insert(requesterRef, id=20, 2),
  Contains(requesterRef, id=80, 1),
  Contains(requesterRef, id=70, 2)
 */
/*
  OperationFinished(id=10),
  OperationFinished(id=20),
  ContainsResult(id=50, false),
  ContainsResult(id=70, true),
  ContainsResult(id=80, false),
  OperationFinished(id=100)
 */
